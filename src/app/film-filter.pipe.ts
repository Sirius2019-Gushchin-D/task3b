import {Pipe, PipeTransform} from '@angular/core';
import {Film} from './film';

@Pipe({
  name: 'filter',
  pure: false
})
export class FilmFilterPipe implements PipeTransform {

  transform(value: Film[],
            searchText: string,
            minYear: number,
            maxYear: number,
            genres: string[],
            countries: string[]):
    Film[] {
    return value.filter(element => {
      const yearMatch = element.year >= minYear && element.year <= maxYear;
      let genreMatchCount = 0;
      if (genres && genres.length !== 0) {
        for (const genre of genres) {
          if (element.genres.includes(genre)) {
            genreMatchCount++;
          }
        }
      }
      const genresMatch = !genres || genres.length === 0 || genreMatchCount === genres.length;

      let countryMatchCount = 0;
      if (countries && countries.length !== 0) {
        for (const country of countries) {
          if (element.countries.includes(country)) {
            countryMatchCount++;
          }
        }
      }
      const countriesMatch = !countries || countries.length === 0 || countryMatchCount === countries.length;

      let searchMatch = false;
      if (searchText && searchText.length !== 0) {
        for (const word of searchText.split(' ')) {
          if (element.title.toLowerCase().includes(word.toLowerCase())) {
            searchMatch = true;
          }
        }
      } else {
        searchMatch = true;
      }
      return yearMatch && genresMatch && countriesMatch && searchMatch;
    });
  }

}
