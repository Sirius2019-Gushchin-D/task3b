import {Injectable} from '@angular/core';
import {Papa} from 'ngx-papaparse';
import {Film} from './film';

@Injectable({
  providedIn: 'root'
})
export class FilmsService {

  constructor(private papa: Papa) {
  }

  private filmList: Film[];


  parseAsync(csv: string) {
    return new Promise((resolve, reject) => {
      this.papa.parse(csv, {
        complete: (result) => {
          const values = ['Title', 'Year', 'Genres', 'Country', 'Raiting', 'Image'];
          for (let i = 0; i < result.data[0].length; i++) {
            if (result.data[0][i] !== values[i]) {
              return reject('Invalid table');
            }
          }

          const films = [];
          for (let i = 1; i < result.data.length; i++) {
            const filmarr = result.data[i];
            if (filmarr.length !== 6) {
              continue;
            }
            const film = new Film(filmarr[0], Number(filmarr[1]), filmarr[2].split(', '), filmarr[3].split(', '), filmarr[4], filmarr[5]);
            films.push(film);
          }
          resolve(films);
        },
        error: (err) => {
          reject(err);
        }
      });
    });
  }

  getAvailableValues(): [number, number, string[], string[]] {
    let minYear = 1.7976931348623157E+308;
    let maxYear = 5E-324;
    const genres = [];
    const countries = [];
    for (const film of this.filmList) {
      if (film.year > maxYear) {
        maxYear = film.year;
      }
      if (film.year < minYear) {
        minYear = film.year;
      }

      for (const genr of film.genres) {
        if (!genres.includes(genr)) {
          genres.push(genr);
        }
      }

      for (const country of film.countries) {
        if (!countries.includes(country)) {
          countries.push(country);
        }
      }
    }
    return [minYear, maxYear, genres, countries];
  }

  async parse(csv: string) {
    try {
      // @ts-ignore
      this.filmList = await this.parseAsync(csv);
      return true;
    } catch (e) {
      return false;
    }
  }

  checkFilmsAvailable(): boolean {
    return this.filmList && this.filmList.length !== 0;
  }

  getFilms(): Film[] {
    return this.filmList;
  }
}
