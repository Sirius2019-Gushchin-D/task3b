import {Component, OnInit} from '@angular/core';
import {FilmsService} from '../films.service';
import {Film} from '../film';
import {Router} from '@angular/router';


@Component({
  selector: 'app-film-menu',
  templateUrl: './film-menu.component.html',
  styleUrls: ['./film-menu.component.css']
})
export class FilmMenuComponent implements OnInit {

  constructor(private filmsService: FilmsService, private router: Router) {
  }


  films: Film[];

  loadIterations = 1;

  yearMin: number;
  yearMax: number;
  genres: string[] = [];
  countries: string[] = [];
  searchText = '';

  availableYearMin: number;
  availableYearMax: number;
  availableGenres: string[] = [];
  availableCountries: string[] = [];

  reFresh() {
    this.films = this.filmsService.getFilms();
  }

  ngOnInit() {
    if (!this.filmsService.checkFilmsAvailable()) {
      return this.router.navigateByUrl('');
    }
    const available = this.filmsService.getAvailableValues();
    this.availableYearMin = available[0];
    this.availableYearMax = available[1];
    this.availableGenres = available[2];
    this.availableCountries = available[3];
    this.yearMax = this.availableYearMax;
    this.yearMin = this.availableYearMin;
    this.reFresh();
  }


  genreChange(genre: string, checked: boolean) {
    if (this.genres.includes(genre) && checked === false) {
      this.genres.splice(this.genres.indexOf(genre), 1);
    } else if (!this.genres.includes(genre) && checked) {
      this.genres.push(genre);
    }
    this.updateResults();
  }

  countryChange(country: string, checked: boolean) {
    if (this.countries.includes(country) && checked === false) {
      this.countries.splice(this.countries.indexOf(country), 1);
    } else if (!this.countries.includes(country) && checked) {
      this.countries.push(country);
    }
    this.updateResults();
  }

  checkYearRange(isMin: boolean) {
    if (this.yearMax < this.yearMin) {
      if (isMin) {
        this.yearMin = this.yearMax;
      } else {
        this.yearMax = this.yearMin;
      }
    }
  }

  private updateResults() {
    // crutch
    const v1 = this.searchText;
    this.searchText += 'a';
    this.searchText = v1;
  }
}
