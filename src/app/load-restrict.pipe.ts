import {Pipe, PipeTransform} from '@angular/core';

@Pipe({
  name: 'loadRestrict'
})
export class LoadRestrictPipe implements PipeTransform {

  transform(value: any[], restrictTo: number): any[] {
    return value.filter((value1, index) => index < restrictTo * 6);
  }

}
