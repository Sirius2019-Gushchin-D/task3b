export class Film {
  title: string;
  year: number;
  genres: string[];
  countries: string[];
  rating: string;
  coverUrl: string;

  constructor(title: string,
              year: number,
              genres: string[],
              countries: string[],
              rating: string,
              coverUrl: string
  ) {
    this.title = title;
    this.year = year;
    this.genres = genres;
    this.countries = countries;
    this.rating = rating;
    this.coverUrl = coverUrl;
  }
}
