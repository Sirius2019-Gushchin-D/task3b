import {Component, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {Router} from '@angular/router';
import {FilmsService} from '../films.service';
import {MatSnackBar} from '@angular/material';

@Component({
  selector: 'app-first',
  templateUrl: './first.component.html',
  styleUrls: ['./first.component.css']
})
export class FirstComponent implements OnInit {

  constructor(
    private router: Router,
    private filmService: FilmsService,
    private snackBar: MatSnackBar
  ) {
  }


  fileChanged(event) {
    const file = event.srcElement.files[0];
    const reader = new FileReader();
    reader.onloadend = async () => {
      if (await this.filmService.parse(reader.result)) {
        this.snackBar.open('Films parsed', 'OK');
        this.router.navigateByUrl('films');
      } else {
        this.snackBar.open('Invalid csv file', 'OK');
        this.router.navigateByUrl('');
      }
    };
    reader.readAsText(file);
  }

  ngOnInit() {
  }

}
