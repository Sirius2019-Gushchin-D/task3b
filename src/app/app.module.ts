import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {PapaParseModule} from 'ngx-papaparse';
import {AppComponent} from './app.component';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {FirstComponent} from './first/first.component';
import {FormsModule} from '@angular/forms';


import {MatToolbarModule} from '@angular/material';
import {MatButtonModule} from '@angular/material';
import {MatSidenavModule} from '@angular/material';
import {MatExpansionModule} from '@angular/material';
import {MatCardModule} from '@angular/material/card';
import {MatSnackBarModule} from '@angular/material';
import {MatCheckboxModule} from '@angular/material';
import {MatSliderModule} from '@angular/material';
import {MatInputModule} from '@angular/material';
import {MatGridListModule} from '@angular/material/grid-list';


import {RouterModule, Routes} from '@angular/router';
import {FilmMenuComponent} from './film-menu/film-menu.component';
import {FilmCardComponent} from './film-card/film-card.component';
import {FilmFilterPipe} from './film-filter.pipe';
import {LoadRestrictPipe} from './load-restrict.pipe';

const appRoutes: Routes = [
  {path: '', component: FirstComponent},
  {path: 'films', component: FilmMenuComponent},
];

@NgModule({
  declarations: [
    AppComponent,
    FirstComponent,
    FilmMenuComponent,
    FilmCardComponent,
    FilmFilterPipe,
    LoadRestrictPipe
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    PapaParseModule,
    FormsModule,
    MatToolbarModule,
    MatButtonModule,
    MatSidenavModule,
    MatExpansionModule,
    MatCardModule,
    MatSliderModule,
    MatSnackBarModule,
    MatCheckboxModule,
    MatInputModule,
    MatGridListModule,
    RouterModule.forRoot(appRoutes)
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {
}
